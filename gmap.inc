<?php
/**
 * @file
 * Webform gmap core.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_gmap() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'mandatory' => FALSE,
    'pid' => 0,
    'weight' => 0,
    'value' => NULL,
    'extra' => array(
      'private' => FALSE,
      'description' => '',
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_gmap() {
  return array(
    'webform_gmap' => array(
      'render element' => 'element',
      'file' => 'gmap.inc',
    ),
    'webform_display_gmap' => array(
      'render element' => 'element',
      'file' => 'gmap.inc',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_gmap($component) {
  $form = array();
  $defaults = gmap_defaults();

  $form['extra']['street'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#default_value' => isset ($component['extra']['street']) ? $component['extra']['street'] : $defaults['street'],
    '#description' => t('The key field for street address textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['housenumber'] = array(
    '#type' => 'textfield',
    '#title' => t('House Number Street'),
    '#default_value' => isset ($component['extra']['housenumber']) ? $component['extra']['housenumber'] : $defaults['housenumber'],
    '#description' => t('The key field for House Number Street textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['postcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Postcode'),
    '#default_value' => isset ($component['extra']['postcode']) ? $component['extra']['postcode'] : $defaults['postcode'],
    '#description' => t('The key field for postcode textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => isset ($component['extra']['city']) ? $component['extra']['city'] : $defaults['city'],
    '#description' => t('The key field for city textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['district'] = array(
    '#type' => 'textfield',
    '#title' => t('District'),
    '#default_value' => isset ($component['extra']['district']) ? $component['extra']['district'] : $defaults['district'],
    '#description' => t('The key field for district textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['province'] = array(
    '#type' => 'textfield',
    '#title' => t('Province'),
    '#default_value' => isset ($component['extra']['province']) ? $component['extra']['province'] : $defaults['province'],
    '#description' => t('The key field for province textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#default_value' => isset ($component['extra']['latitude']) ? $component['extra']['latitude'] : $defaults['latitude'],
    '#description' => t('The key field for custom latitude textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#default_value' => isset ($component['extra']['longitude']) ? $component['extra']['longitude'] : $defaults['longitude'],
    '#description' => t('The key field for custom longitude textfield'),
    '#size' => 60,
    '#maxlength' => 127,
  );
  $form['extra']['txtcss'] = array(
    '#type' => 'textfield',
    '#title' => t('Css for msg'),
    '#default_value' => isset ($component['extra']['txtcss']) ? $component['extra']['txtcss'] : 'color:red',
    '#description' => t('Type css string for the message. It show after move marker. E.g. color:red; font-size:13px;'),
    '#size' => 60,
    '#maxlength' => 130,
  );
  $form['extra']['csswas'] = array(
    '#type' => 'textfield',
    '#title' => t('Css for old value after marker change'),
    '#default_value' => isset ($component['extra']['csswas']) ? $component['extra']['csswas'] : 'color:grey',
    '#description' => t('Type css string for the message below input field after move marker. It show old value input field. E.g. color:grey; font-size:13px;'),
    '#size' => 60,
    '#maxlength' => 130,
  );
  $form['extra']['opt_map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Map Options'),
  );
  $form['extra']['opt_map']['center'] = array(
  		'#type' => 'textfield',
  		'#title' => t('Default center'),
  		'#default_value' => isset ($component['extra']['opt_map']['center']) ? $component['extra']['opt_map']['center'] : '',
  		'#description' => t('Center of the map. Please set value in form e.g. 40.131641,18.326569'),
  		'#size' => 60,
  		'#maxlength' => 127,
  		'#weight' => 0,
  );
  $form['extra']['opt_map']['alert_gmap_zoom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Gmap zoom out alert'),
    '#default_value' => isset($component['extra']['opt_map']['alert_gmap_zoom']) ? $component['extra']['opt_map']['alert_gmap_zoom'] : '0',
    '#description' => 'Check if you want alert when zoom out after next value "Gmap Zoom at init map".',
  );
  $form['extra']['opt_map']['gmapzoom'] = array(
    '#type' => 'textfield',
  	'#title' => t('Gmap zoom at init map'),
  	'#default_value' => isset ($component['extra']['opt_map']['gmapzoom']) ? $component['extra']['opt_map']['gmapzoom'] : '10',
  	'#description' => t('Type gmap zoom when map is initializate. Value 0 - 19. 0 lowest zoom (whole world) 19 highest zoom (individual buildings, if available)'),
  	'#size' => 5,
  	'#maxlength' => 2,
  );
  $form['extra']['opt_map']['gmapzoombtn'] = array(
    '#type' => 'textfield',
    '#title' => t('Gmap zoom when press Button for find location'),
    '#default_value' => isset ($component['extra']['opt_map']['gmapzoombtn']) ? $component['extra']['opt_map']['gmapzoombtn'] : '17',
    '#description' => t('Type gmap zoom when map is initializate. Value 0 - 19. 0 lowest zoom (whole world) 19 highest zoom (individual buildings, if available)'),
    '#size' => 5,
    '#maxlength' => 2,
  );
  $form['extra']['opt_map']['check_bounds'] = array(
  		'#type' => 'checkbox',
  		'#title' => t('Gmap Bounds'),
  		'#default_value' => isset($component['extra']['opt_map']['check_bounds']) ? $component['extra']['opt_map']['check_bounds'] : '0',
  		'#description' => 'Limit view map at rectangle into next value point "South West Point Bounds".',
  );
  $form['extra']['opt_map']['sw'] = array(
  		'#type' => 'textfield',
  		'#title' => t('South West Point Bounds'),
  		'#default_value' => isset ($component['extra']['opt_map']['sw']) ? $component['extra']['opt_map']['sw'] : '',
  		'#description' => t('Set first point of rectangle gmap. Please set value in form e.g. 40.131641,18.326569'),
  		'#size' => 30,
  		'#maxlength' => 40,
  );
  $form['extra']['opt_map']['ne'] = array(
  		'#type' => 'textfield',
  		'#title' => t('North East Point Bounds'),
  		'#default_value' => isset ($component['extra']['opt_map']['ne']) ? $component['extra']['opt_map']['ne'] : '',
  		'#description' => t('Set second point of rectangle gmap. Please set value in form e.g. 40.131641,18.326569'),
  		'#size' => 30,
  		'#maxlength' => 40,
  );
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_gmap($component, $value = NULL, $filter = TRUE) {
  $element = array();
  $element['webform_gmap_button'] = array(
    '#type' => 'button',
    '#value' => t('Find address into map and set coordinate'),
    '#id' => 'findintomap',
    '#name' => 'findintomap',
  );
  $msg_css = $component['extra']['txtcss'] ? $component['extra']['txtcss'] : '';
  $element['webform_gmap_msg'] = array(
    '#markup' => "<div id=\"msggmap\" style=\"$msg_css\" ></div>",
  );
  $element['webform_gmap'] = array(
    '#type' => 'webform_gmap',
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description']) : $component['extra']['description'],
    '#element_validate' => array('webform_validate_gmap'),
    '#default_value' => $filter ? _webform_filter_values($component['value'], NULL, NULL, NULL, FALSE) : $component['value'],
    '#theme' => 'webform_gmap',
    '#theme_wrappers' => array('webform_element'),
    '#pre_render' => array('webform_element_title_display'),
    '#post_render' => array('webform_element_wrapper'),
    '#webform_component' => $component,
  );
  if ($component['extra']['latitude'] == '') {
    $element['latitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#default_value' => $default_values['latitude'],
    );
  }
  if ($component['extra']['longitude'] == '') {
    $element['longitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#default_value' => $default_values['longitude'],
    );
  }
  $latlong = explode(',', $component['extra']['opt_map']['center']);
  $map_array1 = array(
    'id' => gmap_get_auto_mapid(),
    'width' => "82%",
    'height' => "400px",
    'latitude' => $latlong[0],
    'longitude' => $latlong[1],
    'zoom' => $component['extra']['opt_map']['gmapzoom'],
    'maptype' => "Map",
    'controltype' => "Small",      
    'behavior' => array(
      'nodrag' => FALSE,
      'nokeyboard' => FALSE,
      'nomousezoom' => TRUE,
    ),
  );
  // note: behavior['nodrag'] not work properly. i use into js
  // marker.opts.draggable=true;
  $element['map'] = array(
    '#type' => 'gmap',
    '#gmap_settings' => $map_array1,
  );
  $msid = _webform_fetch_draft_sid($component['nid'], $GLOBALS['user']->uid);
  return $element;
}

/**
 * Theme a webform gmap.
 */
function theme_webform_gmap($element) {
  if ($element['element']['#webform_component']['extra']['opt_map']['sw'])
    $sw = explode(',', $element['element']['#webform_component']['extra']['opt_map']['sw']);
  if ($element['element']['#webform_component']['extra']['opt_map']['ne'])
  	$ne = explode(',', $element['element']['#webform_component']['extra']['opt_map']['ne']);
  /*
  *PLEASE ATTENTION
  * The keys of this dictionary must be equal at the results array googlemaps
  * used for the reversing geocode into webform_gmap.js
  * The order of this dictionary is used for make the address to send at
  * google for geocode and return the marker into webform_gmap.js
  * Google use district in the results array (often when
  * result[0].geometry.location_type="ROOFTOP") and the name of type is the
  * same at city: locality. For this reason i call disctrict the key and
  * remember it into js file
  *
  */
  drupal_add_js(array(
    'webform_gmap' => array(
      'fieldsmap' => array(
        'route' => ($element['element']['#webform_component']['extra']['street']) ? $element['element']['#webform_component']['extra']['street'] : '',
        'street_number' => ($element['element']['#webform_component']['extra']['housenumber']) ? $element['element']['#webform_component']['extra']['housenumber'] : '',
        'postal_code' => ($element['element']['#webform_component']['extra']['postcode']) ? $element['element']['#webform_component']['extra']['postcode'] : '',
        'locality' => ($element['element']['#webform_component']['extra']['city']) ? $element['element']['#webform_component']['extra']['city'] : '',
        'district' => ($element['element']['#webform_component']['extra']['district']) ? $element['element']['#webform_component']['extra']['district'] : '',
        'administrative_area_level_2' => ($element['element']['#webform_component']['extra']['province']) ? $element['element']['#webform_component']['extra']['province'] : '',
      ),
      'coordinate' => array(
        'latitude' => ($element['element']['#webform_component']['extra']['latitude']) ? $element['element']['#webform_component']['extra']['latitude'] : 'latitude',
        'longitude' => ($element['element']['#webform_component']['extra']['longitude']) ? $element['element']['#webform_component']['extra']['longitude'] : 'longitude',
      ),
      'zoom' => array(
        'msgMaxGmapZoom' => t('You cannot zoom out any further.'),
        'gmapZoom' => ($element['element']['#webform_component']['extra']['opt_map']['gmapzoom']) ? $element['element']['#webform_component']['extra']['opt_map']['gmapzoom'] : '',
        'alertGmapZoom' => ($element['element']['#webform_component']['extra']['opt_map']['alert_gmap_zoom']) ? $element['element']['#webform_component']['extra']['opt_map']['alert_gmap_zoom'] : '',
        'gmapzoombtn' => ($element['element']['#webform_component']['extra']['opt_map']['gmapzoombtn']) ? $element['element']['#webform_component']['extra']['opt_map']['gmapzoombtn'] : '',  
      ),
      'bounds' => array(
        'swLat' => ($sw[0]) ? $sw[0] : '',
        'swLng' => ($sw[1]) ? $sw[1] : '',
        'neLat' => ($ne[0]) ? $ne[0] : '',
        'neLng' => ($ne[1]) ? $ne[1] : '',
        'check' => ($element['element']['#webform_component']['extra']['opt_map']['check_bounds']) ? $element['element']['#webform_component']['extra']['opt_map']['check_bounds'] : '',
        'msgErrorBound' => t('You have put the marker out of limit map. Please retype address and press button again.'),
      ),
      'msgMarker' => t('You have changed field address. Check it again.'),
      'msgWas' => t('Was:'),
      'cssWas' => ($element['element']['#webform_component']['extra']['csswas']) ? $element['element']['#webform_component']['extra']['csswas'] : '',
      ),
    ),
    array('type' => 'setting')
  );
  drupal_add_js(drupal_get_path('module', 'webform_gmap') . '/webform_gmap.js');
}

/**
 * Implements hook_gmap().
 */
function webform_gmap_gmap($op, &$map) {
  // For documentation on this hook, look at the
  // gmap.php file in the gmap module.
  switch ($op) {
    case 'behaviors':
      return array(
        'geolocate_auto' => array(
          'title' => t('Geolocate user automatically'), 'default' => FALSE,
          'help' => t('This will geolocate the user on each page load and focus the map there.'),
        ),
        'geolocate_block' => array(
          'title' => t('Geolocate user with block link'),
          'default' => FALSE,
          'help' => t('This will geolocate the user only if they use the action on the GMap Geolocate block.'),
        ),
      );
  }
}



function webform_validate_gmap($element, &$form_state) {
  
}

/*

/**
 * Implements _webform_submit_component().
 *-/

/**
 * Implements _webform_display_component().
 *-/
function _webform_display_gmap($component, $value, $format = 'html') {
	return _webform_display_textfield($component, $value, $format);
}

/**
 * Format the output of entered data for this component.
 *-/
function theme_webform_display_gmap($element) {
	// if you want show map into view card
	// if you want show latitude longitude if not specify it with your fields
	//TODO
}


/**
 * Implements _webform_table_component().
 *-/
function _webform_table_gmap($component, $value) {
  //TODO
}

/**
 * Implements _webform_csv_headers_component().
 *-/
function _webform_csv_headers_gmap($component, $export_options) {
  //TODO
}

/**
 * Implements _webform_csv_data_component().
 *-/
function _webform_csv_data_gmap($component, $export_options, $value) {
  //TODO
}
*/
