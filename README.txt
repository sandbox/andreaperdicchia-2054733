CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation

INTRODUCTION
 ------------

Current Maintainer: Andrea Perdicchia <andrea@perdicchia.tk>



INSTALLATION
------------

Follow this steps:
drush dl gmap
drush en gmap
Configure gmap with your gmap keys

put this module into sites/all/modules
Enable it
Into webform module create input field eg address, locality, lat, lng ecc...
Remeber the key filed!
Create webform map Location and edit it.
Write into txtField the key id created before.
Into view test it.

TIPS
------------
Use http://koti.mbnet.fi/ojalesa/boundsbox/ for bound box
