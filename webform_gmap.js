(function($) {Drupal.behaviors.webform_gmap = {
  attach: function(context, settings) {
    if ($("div[id$='gmap0']").length > 0){
      Drupal.gmap.addHandler('gmap', function (elem) {
    	  
        $(document).ready(function() {
          var latitude = $("input[name$='[" + settings.webform_gmap.coordinate['latitude'] + "]']").val();
          var longitude = $("input[name$='[" + settings.webform_gmap.coordinate['longitude'] + "]']").val();
          if (latitude != '' && longitude != '') {
            var latlng = new google.maps.LatLng(latitude, longitude);
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                var gmap_id = $("div[id$='gmap0']").attr('id');
                var m = Drupal.gmap.getMap(gmap_id);
                var zoom = Number(settings.webform_gmap.zoom.gmapzoombtn);
                marker = {};
                m.coord = latlng;
                m.map.setCenter(latlng);
                m.map.setZoom(zoom);
                marker.latitude = latitude;
                marker.longitude = longitude;
                marker.opts = {};
                marker.opts.draggable = true;
                marker.markername = 'lblue';
                m.change('move',-1);
                m.change('addmarker', -1, marker);
              }
            });
          }
          else {
// no coordinate found
          }
        });
        var obj = this;
        obj.bind("addmarker", function (marker) {
          google.maps.event.addListener(marker.marker, 'dragstart', function (coord) {
            clearMsgMaps();
          });
          google.maps.event.addListener(marker.marker, 'dragend', function (coord) {
        	var checkB = 'go';
            if (settings.webform_gmap.bounds.check){
          	  if (! checkBounds()){
          		//marker moved out bound. I delete it
          		cleanFields();
          		if (this.map.markers){
          		  var lenmarker = this.map.markers.length;
          		  var center = this.map.getCenter();
          	      for (var i = 0; i < lenmarker; i++) {
                    this.map.markers[i].setMap(null); //Remove the marker from the map
                  }
          	    google.maps.Map.prototype.fitBounds(center);
          	    checkB = 'nop';
          	    $("div[id='msggmap']").html(settings.webform_gmap.bounds.msgErrorBound);
          		}
          		showRectangle();
          	  }
      	    }
            if (checkB === 'go'){
              updateLatLngFields(coord.latLng.lat(),coord.latLng.lng());
              codeLatLng(coord.latLng.lat(),coord.latLng.lng());
              msgMaps();
            }
          });
        });
        obj.bind("init", function () {
    	  var map = obj.map;
    	  google.maps.event.addListener(map, "zoom_changed", function () {
    		if (settings.webform_gmap.zoom.alertGmapZoom){
              var maxZoom = Number(settings.webform_gmap.zoom.gmapZoom);
              var mz = maxZoom+1;
              if (map.getZoom() === mz){
                clearMsgMaps();
              }
              if (map.getZoom() < maxZoom){
        	    $("div[id='msggmap']").html(settings.webform_gmap.zoom.msgMaxGmapZoom);
        	    map.setZoom(maxZoom);
        	  }
            }
    	  });
    	  google.maps.event.addListener(map, "dragend", function () {
    	    if (settings.webform_gmap.bounds.check){
        	  if (! checkBounds()){
        		showRectangle();
        	  }
    	    }
    	  });
    	});
        function showRectangle(){
          var gmap_id = $("div[id$='gmap0']").attr('id');
          var map = Drupal.gmap.getMap(gmap_id);
          if (! map.myRectangle){
            map.myRectangle = new google.maps.Rectangle({
              strokeColor: '#FF0000',
              strokeOpacity: 0.98,
              strokeWeight: 2,
              fillColor: '#FF0000',
              fillOpacity: 0.35,
              map: map.map,
              bounds: new google.maps.LatLngBounds(
                new google.maps.LatLng(settings.webform_gmap.bounds.swLat,settings.webform_gmap.bounds.swLng),
                new google.maps.LatLng(settings.webform_gmap.bounds.neLat,settings.webform_gmap.bounds.neLng))
            }); 
          }
        }
        
        // If the map position is out of range, move it back
        // thanks http://econym.org.uk/gmap/example_range.htm
        function checkBounds() {
          if (settings.webform_gmap.bounds.swLat && settings.webform_gmap.bounds.swLng && settings.webform_gmap.bounds.neLat && settings.webform_gmap.bounds.neLng){
            var allowedBounds = new google.maps.LatLngBounds(
              new google.maps.LatLng(settings.webform_gmap.bounds.swLat,settings.webform_gmap.bounds.swLng),
              new google.maps.LatLng(settings.webform_gmap.bounds.neLat,settings.webform_gmap.bounds.neLng));
            var gmap_id = $("div[id$='gmap0']").attr('id');
            var map = Drupal.gmap.getMap(gmap_id);
            // Perform the check and return if OK
            if (allowedBounds.contains(map.map.getCenter())) {
              return true;
            }
            // It`s not OK, so find the nearest allowed point and move there
            var C = map.map.getCenter();
            var X = C.lng();
            var Y = C.lat();
            var AmaxX = allowedBounds.getNorthEast().lng();
            var AmaxY = allowedBounds.getNorthEast().lat();
            var AminX = allowedBounds.getSouthWest().lng();
            var AminY = allowedBounds.getSouthWest().lat();
            if (X < AminX) {X = AminX;}
            if (X > AmaxX) {X = AmaxX;}
            if (Y < AminY) {Y = AminY;}
            if (Y > AmaxY) {Y = AmaxY;}
            map.map.setCenter(new google.maps.LatLng(Y,X));
            return false;
          }
          else{
            alert('No Rectangle coordinate found. Please check configuration webform_gmap');
            return false;
          }
        }
      });
	function msgMaps() {
          $("div[id='msggmap']").html(settings.webform_gmap.msgMarker);
        }
        function clearMsgMaps() {
          $("div[id='msggmap']").html('');
        }
        
        function cleanFields(){
          $.each(settings.webform_gmap.fieldsmap, function(k,v) {
            if (v) {
              $("input[name$='[" + v + "]']").val('');
            }
          });
        }
      
/*
 * This function populate the fields when you drag the marker into map
 * This function is customizable for italy fields.
 * I've no idea if work for your country, so, you must maybe retouch this function.
 *
 */
       function codeLatLng(lat,lng) {
         var latlng = new google.maps.LatLng(lat,lng);
         geocoder = new google.maps.Geocoder();
         geocoder.geocode({'latLng': latlng}, function(results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
             if (results[0]) {
               $.each(results[0].address_components, function(k,v) {
                 if (this.types[0] == 'locality'){
                   // must search if exist administrative_area_level_3 for swap field
                   var loc_coun = '';
                   var counties = '';
                   $.each(results[0].address_components, function(key,val) {
                     if (this.types[0] == 'administrative_area_level_3') {
                       loc_coun = val.long_name;
                       // break
                       return false;
                     }
                   });
                   $.each(results[1].address_components, function(key,val) {
                     if (this.types[0] == 'locality') {
                       // check with results[1] google
                       check_locality = val.long_name;
                       // break
                       return false;
                     }
                   });
                   locality = v.long_name;
                   if (loc_coun != '' && locality != loc_coun) {
                    counties = locality;
                    locality = loc_coun;
                   }
                   else if (loc_coun == '' && check_locality != locality) {
                     counties = check_locality;
                   }
                   if (counties != '' && counties != locality) {
                     updateField(settings.webform_gmap.fieldsmap['district'],counties);
                     updateField(settings.webform_gmap.fieldsmap['locality'],locality);
                   }
                   else {
                     updateField(settings.webform_gmap.fieldsmap['locality'],locality);
                     updateField(settings.webform_gmap.fieldsmap['district'],'');
                   }
                 }
                 else {
                   if (settings.webform_gmap.fieldsmap[this.types[0]]) {
                     x = settings.webform_gmap.fieldsmap[this.types[0]];
                     updateField(x,v.long_name);
                   }
                 }
               });
             }
             else {
               alert('No results found');
             }
           }
           else {
             alert('Geocoder failed due to: ' + status);
           }
         });
       }

       function updateField(nameField,gmapVal) {
         if ($("input[name$='[" + nameField + "]']").length > 0) {
           if ($("input[name$='[" + nameField + "]']").val() != gmapVal) {
             var oldValue = $("input[name$='[" + nameField + "]']").val();
             $("input[name$='[" + nameField + "]']").val(gmapVal);
             if (oldValue == '') {
               oldValue = 'Vuoto';
             }
             if ($('#was_' + nameField).length > 0) {
               $('#was_' + nameField).html(settings.webform_gmap.msgWas + ' ' + oldValue);
             }
             else {
               $("input[name$='[" + nameField + "]']").after('<div id="was_' + nameField + '" style="' + settings.webform_gmap.cssWas + '">' + settings.webform_gmap.msgWas + ' ' + oldValue + '</div>');
             }
           }
           else {
             if ($('#was_' + nameField).length > 0) {
               $('#was_' + nameField).html('');
             }
           }
         }
       }

       function removeRectangle(map){
         if (map.myRectangle){
           map.myRectangle.setMap(null);
    	 }
       }
       
       function updateLatLngFields(lat,lng) {
         $("input[name$='[" + settings.webform_gmap.coordinate['latitude'] + "]']").val(lat);
         $("input[name$='[" + settings.webform_gmap.coordinate['longitude'] + "]']").val(lng);
       }

       $('#findintomap').live('click',function(e) {
         e.preventDefault();
         var gmap_id = $("div[id$='gmap0']").attr('id');
         var m = Drupal.gmap.getMap(gmap_id);
         clearMsgMaps();
         if (settings.webform_gmap.bounds.check){
           removeRectangle(m); 
         }
         var address_parts = new Array();
         $.each(settings.webform_gmap.fieldsmap, function(k,v) {
           if (v) {
             value = $("input[name$='[" + v + "]']").val();
             if(value) {
               address_parts.push(value);
             }
           }
         });
         var address_string = address_parts.join(', ');
         var geocoder = new google.maps.Geocoder();
         geocoder.geocode({'address': address_string}, function (results, status) {
           if (status == google.maps.GeocoderStatus.OK) {
             var zoom = Number(settings.webform_gmap.zoom.gmapzoombtn);
             marker = {};
             m.change('clearmarkers');
             m.coord = results[0].geometry.location;
             m.map.setCenter(results[0].geometry.location);
             m.map.setZoom(zoom);
             marker.latitude = results[0].geometry.location.lat();
             marker.longitude = results[0].geometry.location.lng();
             marker.opts = {};
             marker.opts.draggable = true;
             marker.markername = 'lblue';
             m.change('move',-1);
             m.change('addmarker', -1, marker);
             updateLatLngFields(marker.latitude,marker.longitude);
             codeLatLng(marker.latitude,marker.longitude)
           }
           else {
             alert(Drupal.t("Your address was not found on Google Maps."));
           }
         });
       });
     }
   }
 }
})(jQuery);
